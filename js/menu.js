


document.querySelector("#menu-button").addEventListener("click", toggleMenu);


function toggleMenu() {
  document.querySelector("#menu").classList.toggle("hidden");

  const hiddenMenu = document.querySelector("#menu").classList.contains("hidden");

  if (hiddenMenu == true) {
    document.querySelector("#menu-button").textContent = "☰";
  } else {
    document.querySelector("#menu-button").textContent = "✗";
  }
}