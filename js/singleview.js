"use strict";
window.addEventListener("load", fetchData);
const id = new URLSearchParams(window.location.search).get("id");

function fetchData(){
    fetch ("http://ariadna.dk/mios/WP/wp-json/wp/v2/rabbit")
    .then(resp => resp.json())
    .then(data=>{
        const postsList = data.map((post)=>{
        const obj = {
            header: post.header,
            image: post.image.guid,
            imgAlt: post.image.post_title,
            primaryText: post.text,
            posted: post.posted,
            secundaryText: post.read_more,
            id: post.id
        }
        return obj;
    })

    displayDetails(postsList);
    });
}

function displayDetails(postsList){
    postsList.forEach(post => {
        if(post.id == id){
         const main = document.querySelector("[data-details=singleView]"); 
         main.querySelector("[data-details=image]").src = post.image; 
         main.querySelector("[data-details=image]").alt = post.imgAlt; 
         main.querySelector("[data-details=header]").textContent = post.header; 
         main.querySelector("[data-details=primary-text]").textContent = post.primaryText; 
         main.querySelector("[data-details=secundary-text]").textContent = post.secundaryText; 
        }
    });
}