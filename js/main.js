"use strict";
window.addEventListener("load", start);

let currentPage = 1;
let rows = 3;

 function start(){
    randomSlide();
    fetch ("https://ariadna.dk/mios/WP/wp-json/wp/v2/rabbit")
    .then(resp => resp.json())
    .then(data=>{
        // console.log(data, "data");
            const postsList = data.map((post)=>{
        const obj = {
            header: post.header,
            image: post.image.guid,
            imgAlt: post.image.post_title,
            text: post.text,
            posted: post.posted,
            id: post.id
        }
        return obj;
    })

    displayPosts(postsList, currentPage);
    setUpPagination(postsList);
    searchPost(postsList);
    });

}




function displayPosts(postsList, currentPage){
 
    const postsWrapper = document.querySelector(".postsWrapper");
    postsWrapper.innerHTML= "";
    const template = document.querySelector("[data-details=listView]");

    currentPage--;

    let start = rows * currentPage;
    let end = start + rows;
    let shownItems = postsList.slice(start, end);

    shownItems.forEach((object) => {
        const clon = template.cloneNode(true).content;
        clon.querySelector("[data-details=header]").textContent = object.header;
        clon.querySelector("[data-details=posted]").textContent = object.posted;
        clon.querySelector("[data-details=primary-text]").textContent = object.text;
        clon.querySelector("[data-details=image]").src = object.image;
        clon.querySelector("[data-details=image]").alt = object.imgAlt;

        clon.querySelector("button").addEventListener("click", ()=>{
            location.href = "singleview.html?id=" + object.id;
        })

        postsWrapper.appendChild(clon);
    });

}

function setUpPagination(postsList){
const paginationWrapper = document.querySelector(".pagination");
paginationWrapper.innerHTML = "";
let page = Math.ceil(postsList.length/ rows);

for(let i=1; i< page +1; i++){
    const btn = paginationButton(postsList, i);
    paginationWrapper.appendChild(btn);
}


}

function paginationButton(postsList, page){
        const button = document.createElement("button");
        button.innerText = page;
        if(currentPage === page) button.classList.add("active");
        button.addEventListener("click", ()=>{ 
            currentPage = page;
            displayPosts(postsList, currentPage);
            let lastActiveBtn = document.querySelector(".pagination button.active");
            lastActiveBtn.classList.remove("active");
            button.classList.add("active");
        })
        return button;
}


function randomSlide(){

    fetch ("https://ariadna.dk/mios/WP/wp-json/wp/v2/slides")
    .then(resp => resp.json())
    .then(data=>{
        const slides = data.map((slide)=>{
        const img = {
            image: slide.splash.guid,
            img_alt: slide.splash.post_title
        }
        return img;
    })

    displaySlide(slides);
    });
}

function displaySlide(slides){
    const i = Math.floor(Math.random() * 5);
    document.querySelector(".splash").src = slides[i].image;
    document.querySelector(".splash").alt = slides[i].img_alt;
}



function searchPost(postsList){

    document.querySelector(".search").addEventListener("keyup", (e)=>{
    const value = e.target.value;
    const searched = postsList.filter(post=>{
        return post.header.includes(value.toLowerCase());
    })

    console.log("I don't know how to highlight the text");
    })
}
